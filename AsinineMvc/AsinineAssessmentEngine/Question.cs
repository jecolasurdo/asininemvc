﻿using System.Collections.Generic;
using System.Linq;

namespace AsinineAssessmentEngine
{
    public class Question
    {
        public string Text { get; private set; }

        public List<PossibleResponse> ChosenResponses
        {
            get
            {
                return PossibleResponses.Where((pr) => pr.IsChosen == true).ToList();
            }
        }
        public List<PossibleResponse> PossibleResponses { get; private set; }
        public Question(string text, List<PossibleResponse> possibleResponses)
        {
            Text = text;
            PossibleResponses = possibleResponses;
        }
    }
}
