﻿using System;
using System.Collections.Generic;
using System.Linq;
using AsinineAssessmentEngine.Exceptions;

namespace AsinineAssessmentEngine
{
    public class Assesment
    {
        private string[] _possibleResults;
        private QuestionList _questions;
      
        public Assesment(QuestionList questions, String[] possibleResults)
        {
            _questions = questions;
            _possibleResults = possibleResults;
        }

        public String EvaluateRidiculousness()
        {
            ValidateInputs();
            return GetResult(_questions, _possibleResults);
        }

        private string GetResult(QuestionList questionList, string[] possibleResults)
        {
            var range = questionList.MaximumWeight - questionList.MinimumWeight;
            var bandSize = range / (possibleResults.Length);
            var resultsDictionary = new List<KeyValuePair<float, string>>();
            for (var i = possibleResults.Length - 1; i >= 0; i--)
            {
                resultsDictionary.Add(new KeyValuePair<float, string>(i * bandSize, possibleResults[i]));
            }
            foreach (var result in resultsDictionary)
            {
                if (questionList.AverageChosenWeight >= result.Key)
                {
                    return result.Value;
                }
            }
            return null;
        }

        private void ValidateInputs()
        {
            if (_possibleResults.Length == 0)
            {
                throw new InvalidPossibleResultException();
            }

            if (_questions.Count == 0)
            {
                throw new InvalidQuestionException();
            }

            if (_questions.Any(q => q.PossibleResponses.Count(pr => pr.IsChosen) != 1))
            {
                throw new InvalidResponseException();
            }
        }
    }
}