﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AsinineAssessmentEngine
{
    public class QuestionList : List<Question>
    {
        public Single MinimumWeight
        {
            get
            {
                if (this.Any(question => question.PossibleResponses.Any()))
                {
                    var minvalue = this[0].ChosenResponses[0].Weight;
                    return (from q in this from pr in q.PossibleResponses select pr.Weight).Concat(new[] {minvalue}).Min(); 
                }
                else
                {
                    return 0;
                }
            }
        }

        public Single MaximumWeight
        {
            get
            {
                //return this.SelectMany(question => question.PossibleResponses).Sum(response => response.Weight);
                if (this.Any(question => question.PossibleResponses.Any()))
                {
                    var minvalue = this[0].ChosenResponses[0].Weight;
                    return (from q in this from pr in q.PossibleResponses select pr.Weight).Concat(new[] { minvalue }).Max();
                }
                return 0;
            }
        }

        public Single AverageChosenWeight
        {
            get
            {
                if (this.Any(question => question.PossibleResponses.Any(pr => pr.IsChosen)))
                {
                    return (Single)this.Average(question => question.ChosenResponses.Sum(cr => cr.Weight));
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
