﻿namespace AsinineAssessmentEngine
{
    public class PossibleResponse
    {
        public int Weight { get; private set; }
        public string Text { get; private set; }
        public bool IsChosen { get; set; }
        public PossibleResponse(int weight, string text)
        {
            Weight = weight;
            Text = text;
        }
    }
}
