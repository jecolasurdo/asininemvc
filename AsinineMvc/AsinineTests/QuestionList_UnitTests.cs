﻿using System.Collections.Generic;
using AsinineAssessmentEngine;
using AsinineAssessmentEngine.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AsinineTests
{
    [TestClass]
    public class QuestionList_UnitTests
    {
        [TestMethod]
        public void AverageChosenWeight_NoResponsesPresent_IsZero()
        {
            var questionList = new QuestionList() 
            {
                new Question("Some Text", new List<PossibleResponse>())
            };
            var expectedResult = 0;
            var actualResult = questionList.AverageChosenWeight;
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void AverageChosenWeight_NoChosenResponse_IsZero()
         {
            var questionList = new QuestionList()
            {
                new Question("Some Text", new List<PossibleResponse>()
                {
                    new PossibleResponse(10, "PossibleResponse1") {IsChosen = false},
                    new PossibleResponse(10, "PossibleResponse2") {IsChosen = false}
                })
            };

            var expectedResult = 0;
            var actualResult = questionList.AverageChosenWeight;

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void AverageChosenWeight_ResponsesChosen_AveragesCorrectly()
        {
            var questionList = new QuestionList()
            {
                new Question("Question 1", new List<PossibleResponse>()
                {
                    new PossibleResponse(10, "PossibleResponse1a") {IsChosen = true},
                    new PossibleResponse(99, "PossibleResponse1b") {IsChosen = false}
                }),
                new Question("Question 2", new List<PossibleResponse>()
                {
                    new PossibleResponse(20, "PossibleResponse2a") {IsChosen = true},
                    new PossibleResponse(99, "PossibleResponse2b") {IsChosen = false}
                }),
                new Question("Question 3", new List<PossibleResponse>()
                {
                    new PossibleResponse(60, "PossibleResponse3a") {IsChosen = true},
                    new PossibleResponse(99, "PossibleResponse3b") {IsChosen = false}
                })
            };

            var expectedResult = 30;
            var actualResult = questionList.AverageChosenWeight;

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void AverageChosenWeight_ResponsesChosen_AveragesCorrectly2()
        {
            var questionList = new QuestionList()
            {
                new Question("Question 1", new List<PossibleResponse>()
                {
                    new PossibleResponse(10, "PossibleResponse1a") {IsChosen = false},
                    new PossibleResponse(99, "PossibleResponse1b") {IsChosen = true}
                }),
                new Question("Question 2", new List<PossibleResponse>()
                {
                    new PossibleResponse(20, "PossibleResponse2a") {IsChosen = false},
                    new PossibleResponse(99, "PossibleResponse2b") {IsChosen = true}
                }),
                new Question("Question 3", new List<PossibleResponse>()
                {
                    new PossibleResponse(60, "PossibleResponse3a") {IsChosen = false},
                    new PossibleResponse(99, "PossibleResponse3b") {IsChosen = true}
                })
            };

            var expectedResult = 99;
            var actualResult = questionList.AverageChosenWeight;

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void AverageChosenWeight_ResponsesChosen_AveragesCorrectly3()
        {
            var questionList = new QuestionList()
            {
                new Question("Question 1", new List<PossibleResponse>()
                {
                    new PossibleResponse(100, "PossibleResponse1a") {IsChosen = false},
                    new PossibleResponse(150, "PossibleResponse1b") {IsChosen = true}
                }),
                new Question("Question 2", new List<PossibleResponse>()
                {
                    new PossibleResponse(100, "PossibleResponse2a") {IsChosen = false},
                    new PossibleResponse(150, "PossibleResponse2b") {IsChosen = true}
                }),
                new Question("Question 3", new List<PossibleResponse>()
                {
                    new PossibleResponse(100, "PossibleResponse3a") {IsChosen = false},
                    new PossibleResponse(150, "PossibleResponse3b") {IsChosen = true}
                })
            };

            var expectedResult = 150;
            var actualResult = questionList.AverageChosenWeight;

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void MinWeight_Values0through4_Returns0()
        {
            var questionList = new QuestionList()
            {
                new Question("Question 1", new List<PossibleResponse>()
                {
                    new PossibleResponse(0, "PossibleResponse1a") {IsChosen = false},
                    new PossibleResponse(2, "PossibleResponse1b") {IsChosen = true}
                }),
                new Question("Question 2", new List<PossibleResponse>()
                {
                    new PossibleResponse(1, "PossibleResponse2a") {IsChosen = false},
                    new PossibleResponse(3, "PossibleResponse2b") {IsChosen = true}
                }),
            };

            var expectedResult = 0;
            var actualResult = questionList.MinimumWeight;

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void MinWeight_Values1through4_Returns0()
        {
            var questionList = new QuestionList()
            {
                new Question("Question 1", new List<PossibleResponse>()
                {
                    new PossibleResponse(5, "PossibleResponse1a") {IsChosen = false},
                    new PossibleResponse(2, "PossibleResponse1b") {IsChosen = true}
                }),
                new Question("Question 2", new List<PossibleResponse>()
                {
                    new PossibleResponse(1, "PossibleResponse2a") {IsChosen = false},
                    new PossibleResponse(3, "PossibleResponse2b") {IsChosen = true}
                }),
            };

            var expectedResult = 1;
            var actualResult = questionList.MinimumWeight;

            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
