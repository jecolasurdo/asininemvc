﻿using System.Collections.Generic;
using AsinineAssessmentEngine;
using AsinineAssessmentEngine.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AsinineTests
{
    [TestClass]
    public class Assesment_UnitTests
    {
        [TestMethod]
        [ExpectedException(typeof (InvalidPossibleResultException))]
        public void EvaluateRidiculousness_PossibleResultsEmpty_ThrowsInvalidPossibleResultException()
        {
            var assesment = new Assesment(new QuestionList(), new String[0]);
            assesment.EvaluateRidiculousness();
        }

        [TestMethod]
        [ExpectedException(typeof (InvalidQuestionException))]
        public void EvaluateRidiculousness_QuestionsEmpty_ThrowsInvalidQuestionException()
        {
            var assesment = new Assesment(new QuestionList(), new String[1] {"Foo"});
            assesment.EvaluateRidiculousness();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidResponseException))]
        public void EvaluateRidiculousness_QuestionHasMoreThanOneResponseChosen_ThrowsInvlidResponseException()
        {
            var questions = new QuestionList() {
                new Question("",new List<PossibleResponse>() {
                    new PossibleResponse(0, "") {IsChosen = true},
                    new PossibleResponse(0, "") {IsChosen = true}
                })
            };
            var assessment = new Assesment(questions, new[] { "Foo" });
            assessment.EvaluateRidiculousness();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidResponseException))]
        public void EvaluateRidiculousness_QuestionHasNoResponsesChosen_ThrowsInvalidResponseException()
        {
            var questions = new QuestionList() {
                new Question("",new List<PossibleResponse>() {
                    new PossibleResponse(0, "") {IsChosen = false}
                })
            };
            var assessment = new Assesment(questions, new[] { "Foo" });
            assessment.EvaluateRidiculousness();
        }

        [TestMethod]
        public void EvaluteRidiculousness_SingleQuestionWithSingleResponse_ReturnsResponse()
        {
            var expectedResult = "Single Result";
            var question = new Question(
                "Question Text",
                new List<PossibleResponse>() { new PossibleResponse(1, "Response Text") {IsChosen = true}});
            var assesment = new Assesment(new QuestionList() { question }, new String[1] { expectedResult });
            Assert.AreEqual(expectedResult, assesment.EvaluateRidiculousness());
        }

        [TestMethod]
        public void EvaluteRidiculousness_OneQuestion_OneResult_OneResponse_ReturnsResult()
        {
            var questions = new QuestionList()
            {
                new Question("", new List<PossibleResponse>() {new PossibleResponse(0, "Yes") {IsChosen = true}})
            };
            var resultOptions = new[] {"One"};
            var assesment = new Assesment(questions, resultOptions);
            Assert.AreEqual("One", assesment.EvaluateRidiculousness());
        }

        [TestMethod]
        public void EvaluteRidiculousness_OneQuestion_TwoResults_OneResponse_ReturnsResult()
        {
            var questions = new QuestionList()
            {
                new Question("", new List<PossibleResponse>()
                {
                    new PossibleResponse(0, "") {IsChosen = true},
                    new PossibleResponse(1, "") {IsChosen = false}
                })
            };
            var resultOptions = new[] { "One" };
            var assesment = new Assesment(questions, resultOptions);
            Assert.AreEqual("One", assesment.EvaluateRidiculousness());
        }

        [TestMethod]
        public void EvaluteRidiculousness_TwoQuestionsWithTwoResults_TwoResponses_RoundsUp()
        {
            var questions = new QuestionList()
            {
                new Question("", new List<PossibleResponse>()
                {
                    new PossibleResponse(0, "") {IsChosen = false},
                    new PossibleResponse(1, "") {IsChosen = true}
                }),
                new Question("", new List<PossibleResponse>()
                {
                    new PossibleResponse(0, "") {IsChosen = true},
                    new PossibleResponse(1, "") {IsChosen = false}
                })
            };
            var resultOptions = new[] { "One", "Two" };
            var assesment = new Assesment(questions, resultOptions);
            Assert.AreEqual("Two", assesment.EvaluateRidiculousness());
        }

        [TestMethod]
        public void EvaluteRidiculousness_TwoQuestionsWithTwoResults_TwoHeavyResponses_ReturnsHeavierResult()
        {
            var questions = new QuestionList()
            {
                new Question("", new List<PossibleResponse>()
                {
                    new PossibleResponse(0, "") {IsChosen = false},
                    new PossibleResponse(1, "") {IsChosen = true}
                }),
                new Question("", new List<PossibleResponse>()
                {
                    new PossibleResponse(0, "") {IsChosen = false},
                    new PossibleResponse(1, "") {IsChosen = true}
                })
            };
            var resultOptions = new[] { "One", "Two" };
            var assesment = new Assesment(questions, resultOptions);
            Assert.AreEqual("Two", assesment.EvaluateRidiculousness());
        }
    }
}