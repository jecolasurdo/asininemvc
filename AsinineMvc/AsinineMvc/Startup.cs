﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AsinineMvc.Startup))]
namespace AsinineMvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
